package ru.drugstore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Дмитрий on 24.09.2017
 */
public class MenuNode implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String link;

    private List<MenuNode> childs = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = "https://samara.piluli.ru" + link;
    }

    public List<MenuNode> getChilds() {
        return childs;
    }

    public void setChilds(List<MenuNode> childs) {
        this.childs = childs;
    }

    @Override
    public String toString() {
        return name + " --> " + link + '\n' +
                "[ " + childs +
                ']';
    }
}
