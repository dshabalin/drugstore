package ru.drugstore;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Helper {

    private WebClient webClient;

    public Helper(WebClient webClient) {
        this.webClient = webClient;
    }

    List<MenuNode> getSubMenu(HtmlPage page) {
        List<HtmlAnchor> menuNodes = page.getByXPath(".//*/a[@class='js-with-drop-link']");
        List<MenuNode> drugMenuNodes = menuNodes.stream().map(item -> {
            MenuNode node = new MenuNode();
            node.setLink(item.getHrefAttribute());
            node.setName(item.getTextContent());
            return node;
        }).collect(Collectors.toList());
        drugMenuNodes.stream().forEach(System.out::println);

        return drugMenuNodes;

    }

    List<MenuNode> buildMenuTree() throws IOException {
        WebClient webClient = new WebClient();
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        String entry = "https://samara.piluli.ru/categories.html";

        final HtmlPage page = webClient.getPage(entry);
        List<MenuNode> menu = getSubMenu(page);

        try {
            buildFlow(menu);
        }catch (IOException e){
            e.printStackTrace();
        }
        return menu;
    }

    private void buildFlow(List<MenuNode> mainMenuItems) throws IOException {
        for (MenuNode node : mainMenuItems) {
            final HtmlPage page = webClient.getPage(node.getLink());
            List<MenuNode> submenu = getSubMenu(page);
            node.setChilds(submenu);
            buildFlow(submenu);
        }
    }
}
