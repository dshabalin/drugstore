package ru.drugstore;

import com.gargoylesoftware.htmlunit.WebClient;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class Parser {

    public static void main(String[] args) throws IOException {
        System.setProperty("https.protocols", "TLSv1.1");
        List<MenuNode> nodes;
        try (final WebClient webClient = new WebClient()) {
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            Helper helper = new Helper(webClient);
            nodes = helper.buildMenuTree();
        }

        FileOutputStream fout = new FileOutputStream("C:\\menu.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(nodes);
    }
}
